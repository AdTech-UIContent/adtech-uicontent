import { Component,OnInit } from '@angular/core';
import { DataService } from '../services/data.service'
import { Http , Response } from '@angular/http';
import { ProductService } from '../services/app.service';
import { SearchItem } from '../data/product'

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-promo1',
  templateUrl: '../templates/promo1.component.html',
  styleUrls: ['../style/component.style.css']
})
export class PromoComponent1 implements OnInit{
  message:Object;
  public loader;
  iproducts: SearchItem[];
  constructor(private data: DataService,private _product: ProductService) { }

  ngOnInit() {
    this.data.currentMessage.subscribe(message => this.message = message)
    this.loader=true;
    this._product.getproducts()
    .subscribe(iproducts => this.iproducts = iproducts);
  }
  ngAfterViewInit():void{
    this.loader=false;
  }
}