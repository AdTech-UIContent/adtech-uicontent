import { Component} from '@angular/core';
import { Router} from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: '../templates/login.component.html',
  styleUrls: ['../style/component.style.css']
  //providers: [ROUTER_PROVIDERS]
})
export class LoginComponent {
  constructor(private router: Router) { }
  submitLogin(){
    console.log("Submitted sucessdully");
    this.router.navigateByUrl('/homePage');
  }
  /*function onSignIn(googleUser) {
  var profile = googleUser.getBasicProfile();
  console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
  console.log('Name: ' + profile.getName());
  console.log('Image URL: ' + profile.getImageUrl());
  console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.
}*/
}
