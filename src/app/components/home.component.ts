import { Component} from '@angular/core';
import { Router} from '@angular/router';
import { ProductService } from '../services/app.service';
import { SearchItem } from '../data/product'
import { ProductList } from '../data/promotion-list'
import { Observable } from 'rxjs/Observable';
import { DataService } from '../services/data.service'

@Component({
  selector: 'app-login',
  templateUrl: '../templates/home.component.html',
  styleUrls: ['../style/component.style.css']
  //providers: [ROUTER_PROVIDERS]
})
export class HomeComponent {
  arr = [];
  message:Object;
  promotionLists: ProductList[];
  private currentValue;
  constructor(private _product: ProductService,private router: Router,private data: DataService) {
    
  }
  ngOnInit() :void {
    //this.loader=true;
    this._product.getProductList().subscribe(promotionLists => this.promotionLists = promotionLists);
    
    this.data.currentMessage.subscribe(message => this.message = message)
  }
  

  ProductListShow(arg1){
    let message="Hello Dharani"
    this.arr=[];
    console.log("my values",arg1.ProductTitle,arg1.ProductId)
    this.arr.push(arg1);
    //this.router.navigateByUrl("/:"+x.ProductId+"/"+x.ProductTitle);
    
     this.data.changeMessage(this.arr);
    
  }

  
  
}