import { Component,OnInit} from '@angular/core';
import { CookieService } from 'angular2-cookie/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']

})
export class AppComponent implements OnInit{
  constructor(private _cookieService:CookieService){}


  ngOnInit() {
    this._cookieService.put("test","my cookies");
    console.log("Set Test Cookie as Test");
    this.getCookie("test");
  }

  getCookie(test:String){
    console.log(this._cookieService.get("test"));
  }
}
