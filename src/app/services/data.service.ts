import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class DataService {

  private messageSource = new BehaviorSubject<Object>([{
   "ProductId": 1,
   "ProductTitle":"First Promotion",
   "imageUrl": "assets/samsung2.jpg",
   "ProductType":"Mobile",
   "ScreenSize": "5.8 inches",
   "OperatingSystem": "Android",
   "StorageCapacity": "64 GB Storage",
   "Features": "4G LTE, Quad‑band, Water Resistant",
   "Description":"Slim in construction for easy handling, this bold smartphone delivers professional quality shots through Pro Mode manual adjustments. The dual aperture lens adjusts to the light, so images show up in crystal clarity on the 5.8-inch screen, while Iris Scanner and Face Recognition provide instant access."
}]);
  currentMessage = this.messageSource.asObservable();

  constructor() { }

  changeMessage(message: Object) {
    this.messageSource.next(message)
  }

}